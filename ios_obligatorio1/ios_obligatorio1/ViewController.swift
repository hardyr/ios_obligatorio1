//
//  ViewController.swift
//  ios_obligatorio1
//
//  Created by SP 23 on 18/4/17.
//  Copyright © 2017 SP 23. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var Table: UITableView!
    @IBOutlet weak var inputDsc: UITextField!
    @IBOutlet weak var inputQty: UITextField!
    
    @IBOutlet weak var noteLabel: UILabel!
    
    var tableData = [TableDataItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        //let item1=TableDataItem()
        //item1.description = "Leche"
        //item1.quantity=2
        //item1.done = false
        //tableData.append(item1)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:    Int) -> Int {
        return (tableData.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->        UITableViewCell {
        let cell = self.Table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomCell

        cell.LabelQty.text = String(tableData[indexPath.row].quantity)
        cell.LabelDsc.text = tableData[indexPath.row].description
        if (tableData[indexPath.row].done == true) {
            cell.backgroundColor = #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)
        } else {
            cell.backgroundColor = #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)
        }
        
        if (tableData.count > 0) {
            noteLabel.alpha = 0
        } else {
            noteLabel.alpha = 1
        }
        
        return (cell)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == UITableViewCellEditingStyle.delete
        {
            let refreshAlert = UIAlertController(title: "Delete", message: "Are you sure you want to delete this item?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action: UIAlertAction!) in
                self.tableData.remove(at: indexPath.row)
                self.Table.reloadData()
                if (self.tableData.count == 0) {
                    self.noteLabel.alpha = 1
                }
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                
            }))
            
            present(refreshAlert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func Add(_ sender: Any) {
        if (inputDsc.text! != "") {
            
            let tableDataItem = TableDataItem()
            tableDataItem.description=inputDsc.text!
            
            if (inputQty.text! == "") {
                tableDataItem.quantity = 1
            } else {
                let a:Int? = Int(inputQty.text!)
                if a != nil {
                    tableDataItem.quantity = a!
                }
            }
            tableDataItem.done = false
            
            inputDsc.text = ""
            inputQty.text = ""
            
            
            
            tableData.append(tableDataItem)
            
            var dictionaries = [[String : String]]()
            if let dic = UserDefaults.standard.object(forKey: "tableData") as?  [[String: String]]
            {
                dictionaries = dic
            }
            
            let dictionary1: [String: String] = ["dsc": tableDataItem.description,"qty":String(tableDataItem.quantity),"done":String(tableDataItem.done)]
            
            dictionaries.append(dictionary1)
            
            UserDefaults.standard.set(dictionaries, forKey: "tableData")
            Table.reloadData()

            
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableData[indexPath.row].done = !tableData[indexPath.row].done
        Table.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let dic = UserDefaults.standard.object(forKey: "tableData") as?  [[String: String]]
        {
            tableData.removeAll()
            for item in dic
            {
                
                let tdi = TableDataItem()
                for (key,value) in item
                {
                    
                    if key=="dsc"
                    {
                        tdi.description=value
                    }
                    
                    if key=="qty"
                    {
                        tdi.quantity=Int(value)!
                    }
                    
                    if key=="done"
                    {
                        tdi.done=Bool(value)!
                    }
                    
                    
                }
                tableData.append(tdi)
                
            }
            Table.reloadData()
        }
    }
    
    
    
    
    
    // Start Editing The Text Field
    func textFieldDidBeginEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -226, up: true)
    }
    
    // Finish Editing The Text Field
    func textFieldDidEndEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -226, up: false)
    }
    
    // Hide the keyboard when the return key pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Move the text field in a pretty animation!
    func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
        let moveDuration = 0.1
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    
}


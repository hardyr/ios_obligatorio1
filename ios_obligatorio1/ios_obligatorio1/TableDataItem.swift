//
//  File.swift
//  ios_obligatorio1
//
//  Created by Gonzalo Barrios on 19/4/17.
//  Copyright © 2017 SP 23. All rights reserved.
//

import Foundation

class TableDataItem: NSCoding {
    var description = ""
    var quantity = 1
    var done = false
    
    init(description: String, quantity: Int, done: Bool) {
        self.description = description
        self.quantity = quantity
        self.done = done
    }
    
    init() {}
    
    required init(coder decoder: NSCoder) {
        self.description = decoder.decodeObject(forKey: "description") as? String ?? ""
        self.quantity = decoder.decodeInteger(forKey: "quantity")
        self.done = decoder.decodeBool(forKey: "done")
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(description, forKey: "description")
        coder.encode(quantity, forKey: "quantity")
        coder.encode(done, forKey: "done")
    }
}
